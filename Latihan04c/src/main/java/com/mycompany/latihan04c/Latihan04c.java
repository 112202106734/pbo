/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.mycompany.latihan04c;
import java.util.Scanner;
/**
 *
 * @author Gadget House
 */
public class Latihan04c {
   
    public static void main(String[] args) {
        System.out.println("Data TesT!");
        System.out.println("=======================");
        Scanner inp = new Scanner(System.in);
        String nama;
        String prodi;
        int nilai;
        System.out.print("Nama\t\t: ");
        nama=inp.nextLine();
        System.out.print("Program Studi\t: ");
        prodi=inp.nextLine();
        System.out.print("Nilai\t\t: ");
        nilai=inp.nextInt();
        Mahasiswa mhs =new Mahasiswa(nama,prodi,nilai);
        System.out.println("Nilai Huruf\t: "+mhs.kategori());
        System.out.println("=======================");
    }
}
