/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.latihan04c;

/**
 *
 * @author Gadget House
 */
public class Mahasiswa {
    String nama;
    String prodi;
    int nilai;
    Mahasiswa(String nama, String prodi, int nilai){
        this.nama=nama;
        this.prodi=prodi;
        this.nilai=nilai;
    }
    char kategori(){
        if (nilai>=85 && nilai<=100){
            return 'a';
        }else if(nilai>=70 && nilai<85){
            return 'B';
        }else if(nilai>=60 && nilai<70){
            return 'C';
        }else if(nilai>=50 && nilai<60){
            return 'D';
        }else if(nilai>=0 && nilai<50){
            return 'E';
        }else {
            return ' ';
        }
    }
}
