/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.latihan04d;
import java.util.Scanner;
/**
 *
 * @author Gadget House
 */
public class Pembayaran {
    String nama;
    String nopel;
    int pakai;
    int harga;
    void total(){
        for(int i=1; i<=pakai; i++){
            if(i<=10){
                harga+=1000;
            }else if(i>10 && i<=20){
                harga+=2000;
            }else{
                harga+=5000;
            }
        }
    }
    void cetak(){
        Scanner input = new Scanner(System.in);
        System.out.println("Perhitungan Biaya Pemakaian Air");
        System.out.println("=============================");
        System.out.print("Nama\t\t: ");
        nama=input.nextLine();
        System.out.print("No. Pelanggan\t: ");
        nopel=input.nextLine();
        System.out.print("Pemakaian Air\t: ");
        pakai=input.nextInt();
        total();
        System.out.println("Biaya Pakai\t: " + harga);
        System.out.println("=============================");
    }
}
